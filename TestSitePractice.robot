*** Settings ***
#Resource       ../resource/Resource.robot
Resource        resource/Resource.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

### SETUP ele roda keyword antes da suite ou antes de um teste
### TEARDOWN ele roda keyword depois da suite ou antes de um teste

*** Variable ***

*** Test Case ***
#Pronto
Cenário 01: Pesquisar produto existente
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "Blouse"
    Então o produto "Blouse" deve ser listado na página de resultado da busca
#Pronto
Cenário 02: Pesquisar produto não existente
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "itemNãoExistente"
    Então a página deve exibir a mensagem "No results were found for your search "itemNãoExistente""
#Pronto
Cenário 03: Listar produtos
    Dado que estou na página home do site
    Quando eu passar o mause por cima da categoria "Women"
    Então as sub categorias de "Woman" devem ser exibidas
    Quando eu clicar na sub categoria "Summer Dresses"
    Então a página com os produtos da sub categoria "Summer Dresses" deve ser exibida

#Pronto
Cenário 04: Adicionar produtos no carrinho
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "t-shirt"
    Então o produto "t-shirt" deve ser listado na página de resultado
    Quando eu clicar no botão "Add to cart"
    Então a tela de confirmação será exibida
    Quando eu clicar no botão "Proceed to checkout"
    Então a tela do carrinho de compras deve ser exibida
#Pronto
Cenário 05: Adicionar produtos no carrinho
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "t-shirt"
    Então o produto "t-shirt" deve ser listado na página de resultado
    Quando eu clicar no botão "Add to cart"
    Então a tela de confirmação será exibida
    Quando eu clicar no botão "Proceed to checkout"
    Então a tela do carrinho de compras deve ser exibida
    Quando eu clicar na imagem de logo
    Dado que estou na página home do site
    Quando eu clicar no ícone carrinho de compras no menu superior direito
    Então a tela do carrinho de compras deve ser exibida
    Quando eu clicar no botão de remoção de produtos (delete)
    Então deve exibir a mensagem "Your shopping cart is empty."
#Pronto
Cenário 06: Adicionar cliente
    Dado que estou na página home do site
    Quando eu clicar no botão "Sign in"
    Então a página de login será exibida
    Quando eu inserir o email
    e clicar no botão "Create na account"
    Então página com os campos de cadastro deve ser exibida
    Quando eu realizar o cadastro do dados obrigatórios
    Então a página de gerenciamento da conta deve ser exibida

# *** Keywords ***
