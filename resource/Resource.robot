*** Settings ***
Library     SeleniumLibrary
Library     String

*** Variable ***
${URL}        http://automationpractice.com

${BROWSER}    chrome

${OPTIONS}    add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")
#Lista
@{USUARIO}    nome1     sobrenome1      senha1  
#Dicionario
&{PESSOA}     nome=TESTE    finalname=FinalTeste    password=123789     company=Prime   endereco=Rua XYZ  tipoMoradia=Apartament  cidade=CWB  cep=36445   telefone=37796131  celular=997179847  

*** Keywords ***
#### Setup e Teardown ####

Abrir navegador
    Open Browser    about:blank     ${BROWSER}  options=${OPTIONS}
    Maximize Browser Window

Fechar navegador
    Close Browser

#### Passo a Passo ####

Dado que estou na página home do site
    Go To               ${URL} 
    Title Should Be     My Store

Quando eu pesquisar pelo produto "${PRODUTO}"
    Input Text      name=search_query    ${PRODUTO}
    Click Element   name=submit_search

Então o produto "${PRODUTO}" deve ser listado na página de resultado da busca
    Wait Until Element Is Visible   css=#center_column > h1
    Title Should Be                 Search - My Store
    Page Should Contain Image       xpath=//*[@id="center_column"]//*[@src='http://automationpractice.com/img/p/7/7-home_default.jpg']
    Page Should Contain Link        xpath=//*[@id="center_column"]//a[@class="product-name"][contains(text(),"${PRODUTO}")]

Então a página deve exibir a mensagem "${MENSAGEM_ALERTA}"
    Wait Until Element Is Visible  xpath=//*[@id="center_column"]/p[@class="alert alert-warning"]
    Element Text Should Be         xpath=//*[@id="center_column"]/p[@class="alert alert-warning"]  ${MENSAGEM_ALERTA}

Quando eu passar o mause por cima da categoria "${CATEGORIA}"
    Mouse Over           xpath=//*[@id="block_top_menu"]/ul/li/*[@title="${CATEGORIA}"]          

Então as sub categorias de "Woman" devem ser exibidas
    Wait Until Element Is Visible    xpath=//*[@id="block_top_menu"]/ul/li[1]/ul/li/*[@title="Tops"]
    Wait Until Element Is Visible    xpath=//*[@id="block_top_menu"]/ul/li[1]/ul/li/*[@title="Dresses"] 

Quando eu clicar na sub categoria "${SUBCATEGORIA}"
    Click Element                    xpath=//*[@id="block_top_menu"]/ul/li[1]//*[@title="${SUBCATEGORIA}"]  

Então a página com os produtos da sub categoria "Summer Dresses" deve ser exibida
    Wait Until Element Is Visible    xpath=//*[@id="center_column"]/h1/span[1]
    Title Should Be                  Summer Dresses - My Store

Então o produto "t-shirt" deve ser listado na página de resultado
    Wait Until Element Is Visible   xpath=//*[@id="center_column"]/ul/li/div/div[2]/h5/a
    Title Should Be                 Search - My Store

Quando eu clicar no botão "Add to cart"
    Mouse Over                      xpath=//*[@id="center_column"]/ul/li/div/div[2]/h5/a
    Click Element                   xpath=//*[@id="center_column"]/ul/li/div/div[2]/div[2]/a[1]/span

Então a tela de confirmação será exibida
    Wait Until Element Is Visible   xpath=//*[@id="layer_cart"]/div[1]/div[1]/h2

Quando eu clicar no botão "Proceed to checkout"
    Click Element                   xpath=//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a/span

Então a tela do carrinho de compras deve ser exibida
    Wait Until Element Is Visible   id=cart_title
    Wait Until Element Is Visible   id=total_price

Quando eu clicar no ícone carrinho de compras no menu superior direito
    Click Element                   xpath=//*[@id="header"]/div[3]/div/div/div[3]/div/a

Quando eu clicar no botão de remoção de produtos (delete)
    Click Element                   id=1_1_0_0 

Então deve exibir a mensagem "${MENSAGEM_CARRINHO}" 
    Wait Until Element Is Visible  xpath=//*[@id="center_column"]/p
    Element Text Should Be         xpath=//*[@id="center_column"]/p[@class="alert alert-warning"]   ${MENSAGEM_CARRINHO}

Quando eu clicar na imagem de logo
    Click Element       xpath=//*[@id="header_logo"]/a/img

Quando eu clicar no botão "Sign in"
    Click Element       xpath=//*[@class="login"]

Então a página de login será exibida
    Wait Until Element Is Visible   id=SubmitCreate

e clicar no botão "Create na account" 
    Click Element                   id=SubmitCreate

Então página com os campos de cadastro deve ser exibida
    Wait Until Element Is Visible   id=account-creation_form

Quando eu realizar o cadastro do dados obrigatórios
    Click Element  id=id_gender1 
    Input Text     id=customer_firstname     ${PESSOA.nome}
    Input Text     id=customer_lastname      ${PESSOA.finalname}
    Input Text     id=passwd                 ${PESSOA.password}
    Click Element  id=days
    Click Element  xpath=//*[@id="days"]/option[2]
    Click Element  id=months
    Click Element  xpath=//*[@id="months"]/option[2]
    Click Element  id=years
    Click Element  xpath=//*[@id="years"]/option[22]
    Input Text     id=firstname             ${PESSOA.nome}
    Input Text     id=lastname              ${PESSOA.finalname}
    Input Text     id=company               ${PESSOA.company}
    Input Text     id=address1              ${PESSOA.endereco}
    Input Text     id=address2              ${PESSOA.tipoMoradia}
    Input Text     id=city                  ${PESSOA.cidade}
    Click Element  id=id_state
    Click Element                           xpath=//*[@id="id_state"]/option[2]
    Input text     id=postcode              ${PESSOA.cep}  
    Input text     id=phone                 ${PESSOA.telefone}
    Input text     id=phone_mobile          ${PESSOA.celular}
    Click Element  xpath=//*[@id="submitAccount"]/span

Então a página de gerenciamento da conta deve ser exibida
    Title Should Be                 My account - My Store
    Wait Until Element Is Visible   xpath=//*[@id="header"]/div[2]/div/div/nav/div[1]/a
    Wait Until Element Is Visible   xpath=//*[@id="center_column"]/p

# Quando eu inserir o email
#     ${EMAIL}                        Generate Random String
#     Input Text  id=email_create     ${EMAIL}teste@mailinator.com 

Quando eu inserir o email
    ${EMAIL}    Criar um email customizado      ${PESSOA.nome}      ${PESSOA.finalname}
    Input Text  id=email_create     ${EMAIL} 

Criar um email customizado
    [Arguments]             ${NOME}     ${SOBRENOME}
    ${STRING_ALEATORIA}     Generate Random String
    ${CUSTOM_EMAIL}         Set Variable    ${NOME}${SOBRENOME}${STRING_ALEATORIA}@mailinator.com
    Log                     ${CUSTOM_EMAIL}
    [Return]                ${CUSTOM_EMAIL}